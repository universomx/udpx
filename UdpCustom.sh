#!/bin/bash
#by alexmod80
#para @lacasitamx
#instalador udp-custom para la app HTTP-CUSTOM 2023
msg () {
BRAN='\033[1;37m' && VERMELHO='\e[31m' && VERDE='\e[32m' && AMARELO='\e[33m'
AZUL='\e[34m' && MAGENTA='\e[35m' && MAG='\033[1;36m' &&NEGRITO='\e[1m' && SEMCOR='\e[0m'
 case $1 in
  -ne)cor="${VERMELHO}${NEGRITO}" && echo -ne "${cor}${2}${SEMCOR}";;
  -ama)cor="${AMARELO}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
  -verm)cor="${AMARELO}${NEGRITO}${VERMELHO}" && echo -e "${cor}${2}${SEMCOR}";;
  -azu)cor="${MAG}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
  -verd)cor="${VERDE}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
  -bra)cor="${BRAN}" && echo -ne "${cor}${2}${SEMCOR}";;
  "-bar2"|"-bar")cor="\e[1;30m————————————————————————————————————————————————————" && echo -e "${SEMCOR}${cor}${SEMCOR}";;
  -tit)echo -e "\e[91m≪━━─━━─━─━─━─━─━━─━━─━─━─◈─━━─━─━─━─━━─━─━━─━─━━─━≫ \e[0m\n   \e[2;97m\e[3;93m❯❯❯❯❯❯ ꜱᴄʀɪᴩᴛ ᴍᴏᴅ ʟᴀᴄᴀꜱɪᴛᴀᴍx ❮❮❮❮❮❮\033[0m \033[1;31m[\033[1;32mV9Xmod\n\e[91m≪━━─━─━━━─━─━─━─━─━━─━─━─◈─━─━─━─━─━━━─━─━─━━━─━─━≫   \e[0m" && echo -e "${SEMCOR}${cor}${SEMCOR}";;
 esac
}
instalar_udp(){
clear
msg -tit

if [[ -e /root/udp/udp-custom ]]; then
	clear
	msg -bar
	msg -verd "	SERVICIO UDP CUSTOM YA ACTIVO"
	msg -bar
	else
	mkdir -p /root/udp
	msg -bar
	msg -verd "	Descargando binario UDP-CUSTOM ----"
	# change to time GMT+7
#ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
msg -bar
# install udp-custom
msg -verd "	descargando binario udp-custom"
wget -O /root/udp/udp-custom raw.github.com/lacasitamx/UDPcustom/master/Update/udp-custom &>/dev/null
chmod +x /root/udp/udp-custom
msg -bar
msg -verd "	descargando configuracion"
wget -O /root/udp/config.json raw.github.com/lacasitamx/UDPcustom/master/Update/config.json &>/dev/null
chmod 644 /root/udp/config.json
msg -bar

#clear
#36712
add_fire(){
msg -verd "	ACTIVANDO FIREWALL"
msg -bar
sudo apt install firewalld -y &>/dev/null
sudo firewall-cmd --zone=public --permanent --add-port=1-65535/tcp  &>/dev/null
sudo firewall-cmd --zone=public --permanent --add-port=36712/tcp  &>/dev/null
#sudo firewall-cmd --zone=public --permanent --add-port=$PORT/tcp  &>/dev/null
sudo firewall-cmd --reload  &>/dev/null
sudo firewall-cmd --zone=public --list-ports &>/dev/null
}
echo -e "\e[1;93m	DESEA AGREGAR FIREWALLD CMD AL SISTEMA UDP\e[0m\n	OJO EN ALGUNAS VPS ES NECESARIO AGREGAR LOS PERMISOS FIREWALLD POR EJEMPLO,AZURE,AWS,ETC.."
read -p "Responde [ s | n ]: " add_fire
   [[ "$add_fire" = "s" || "$add_fire" = "S" ]] && add_fire
#msg -tit
cat <<EOF > /etc/systemd/system/udp-custom.service
[Unit]
Description=udp-custom by ePro Dev. Team

[Service]
User=root
Type=simple
ExecStart=/root/udp/udp-custom server --config /root/udp/config.json --exclude 53,5300
WorkingDirectory=/root/udp/
Restart=always
RestartSec=2s

[Install]
WantedBy=default.target
EOF

systemctl enable udp-custom &>/dev/null
systemctl start udp-custom &>/dev/null
#msg -bra "	Port: 36712 , exclude 53,5300"
msg -verd "	SERVICIO INSTALADO"
msg -bra "	PUERTOS DISPONIBLES: 1-65535 y 36712"
msg -bar
fi
}
clear
remove() {
  systemctl stop udp-custom &>/dev/null
  systemctl disable udp-custom &>/dev/null
  rm /etc/systemd/system/udp-custom.service
  rm -f /root/udp/udp-custom
  rm -rf /root/udp
  msg -ama "        Servicio UDPcustom Detenido...."
  }
  [[ $(ps x | grep udp-custom| grep -v grep) ]] && ud="\033[1;32m1-65535 y 36712 |exclude: 53,5300" || ud="\033[1;31m[SERVICIO DESACTIVADO"

[[ $(ps x | grep udp-custom| grep -v grep) ]] && uds="\033[1;32m[ON]" || uds="\033[1;31m[OFF]"
  msg -tit
  
  msg -verd "         BINARIO OFICIAL DE e-Pro Dev"
  
  msg -bar
  msg -ama "\e[93m         INSTALADOR UDP-CUSTOM @Lacasitamx"
  echo -e "         \e[97mSolo Funciona en Ubuntu 20+"
  msg -bar
   echo -e "\e[1;93m	PUERTO UDP: $ud\e[0m"
   msg -bar
  echo -e "  $(msg -verd "[1]")$(msg -verm2 "➛ ")$(msg -azu " Instalar UDPcustom")  $uds"
  echo -e "  $(msg -verd "[2]")$(msg -verm2 "➛ ")$(msg -verm " \e[91mRemover UDPcustom")  "
  echo -e "  $(msg -verd "[0]")$(msg -verm2 "➛ ")$(msg -azu " VOLVER")  "
  msg -bar
  echo -ne "  \033[1;37mSelecione Una Opcion : "
	read opc
  case $opc in
  1)instalar_udp;;
  2)remove;;
  esac  

